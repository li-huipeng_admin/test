package com.jt.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.vo.ItemParamVO;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
public class TestObjectMapper {
    @Test
    public void toJSON() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ItemParamVO itemParamVO = new ItemParamVO();
        itemParamVO.setParamId(1);
        itemParamVO.setParamVals("100");
        ItemParamVO itemParamVO2 = new ItemParamVO();
        itemParamVO2.setParamId(1);
        itemParamVO2.setParamVals("100");
        List list = new ArrayList();
        list.add(itemParamVO);
        list.add(itemParamVO2);
        String json =
                mapper.writeValueAsString(list);

        //JSON转化为对象
        List<ItemParamVO> list2 = mapper.readValue(json, List.class);
        System.out.println(list2);

    }
}
