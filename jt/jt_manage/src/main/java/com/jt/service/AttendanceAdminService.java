package com.jt.service;

import com.jt.pojo.Attendance;

import java.util.List;

public interface AttendanceAdminService {

    List<Attendance> findAttendance();
}
