package com.jt.service;

import com.jt.pojo.Attendance;

import java.util.List;

public interface AttendanceService {
    List<Attendance> attendances();

    void attend(Attendance attendance);
}
