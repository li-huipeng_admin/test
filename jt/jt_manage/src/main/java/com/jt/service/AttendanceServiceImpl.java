package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.AttendanceMapper;
import com.jt.pojo.Attendance;
import com.jt.pojo.User;
import org.aspectj.lang.annotation.AfterReturning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class AttendanceServiceImpl implements AttendanceService {


    @Autowired
    private AttendanceMapper attendanceMapper;
    @Override
    public List<Attendance> attendances() {
        ArrayList<Attendance> list = new ArrayList<>();
        for(User it: UserServiceImpl.userSet){

            QueryWrapper<Attendance> wrapper = new QueryWrapper<>();
            wrapper.eq("username",it.getUsername());
            Attendance e = attendanceMapper.selectOne(wrapper);

            list.add(e);
        }

        return list;
    }

    @Override
    public void attend(Attendance attendance) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date(System.currentTimeMillis());
        String date = sdf.format(now);
        String day1 = date.substring(8, 10);
        Integer day = Integer.valueOf(day1);
        System.out.println(day1);

        QueryWrapper<Attendance> wrapper = new QueryWrapper<>();

//        for (User it: UserServiceImpl.userSet) {
//            attendance.setUsername(it.getUsername());
            if (day==1)attendance.setOne(true);
            if (day==2) attendance.setTwo(true) ;
            if (day==3) attendance.setThree(true) ;
            if (day==4) attendance.setFour(true) ;
            if (day==5) attendance.setFive(true) ;
            if (day==6) attendance.setSix(true) ;
            if (day==7) attendance.setSeven(true) ;
            if (day==8) attendance.setEight(true) ;
            if (day==9) attendance.setNine(true) ;
            if (day==10) attendance.setTen(true) ;
            if (day==11) attendance.setEleven(true) ;
            if (day==12) attendance.setTwelve(true) ;
            if (day==13) attendance.setThirteen(true) ;
            if (day==14) attendance.setFourteen(true) ;
            if (day==15) attendance.setFifteen(true) ;
            if (day==16) attendance.setSixteen(true) ;
            if (day==17) attendance.setSeventeen(true) ;
            if (day==18) attendance.setEightteen(true) ;
            if (day==19) attendance.setNineteen(true) ;
            if (day==20) attendance.setTwenty(true) ;
            if (day==21) attendance.setTwentyone(true) ;
            if (day==22) attendance.setTwentytwo(true) ;
            if (day==23) attendance.setTwentytwo(true) ;
            if (day==24) attendance.setTwentyfour(true) ;
            if (day==25) attendance.setTwentyfive(true) ;
            if (day==26) attendance.setTwentysix(true) ;
            if (day==27) attendance.setTwentyseven(true) ;
            if (day==28) attendance.setTwentyeight(true) ;
            if (day==29) attendance.setTwentynine(true) ;
            if (day==30) attendance.setThirty(true) ;
            if (day==31) attendance.setThirtyone(true) ;
        System.out.println(attendance);
            wrapper.eq("username",attendance.getUsername());
            attendanceMapper.update(attendance,wrapper);

//        }




    }
}
