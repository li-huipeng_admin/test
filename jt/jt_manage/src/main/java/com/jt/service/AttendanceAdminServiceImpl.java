package com.jt.service;

import com.jt.mapper.AttendanceAdminMapper;
import com.jt.pojo.Attendance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttendanceAdminServiceImpl implements AttendanceAdminService{

    @Autowired
    private AttendanceAdminMapper attendanceAdminMapper;
    @Override
    public List<Attendance> findAttendance() {
        List<Attendance> list = attendanceAdminMapper.selectList(null);
        return list;
    }
}
