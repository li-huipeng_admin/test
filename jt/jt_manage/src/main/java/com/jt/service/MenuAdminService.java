package com.jt.service;

import com.jt.pojo.Menu;
import com.jt.pojo.MenuAdmin;

import java.util.List;

public interface MenuAdminService {

    List<MenuAdmin> findMenusList();
}
