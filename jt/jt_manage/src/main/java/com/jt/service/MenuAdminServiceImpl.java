package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.MenuAdminMapper;
import com.jt.mapper.MenuMapper;
import com.jt.pojo.Menu;
import com.jt.pojo.MenuAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MenuAdminServiceImpl implements MenuAdminService{
    @Autowired
    private MenuAdminMapper menuAdminMapper;
    @Override
    public List<MenuAdmin> findMenusList() {
        //1.查询一级菜单数据
        QueryWrapper<MenuAdmin> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id",0);
        List<MenuAdmin> oneList =
                menuAdminMapper.selectList(wrapper);
        //2.如何查询二级菜单  父子关系的封装!!!
        for (MenuAdmin oneRights : oneList){
            //查询该元素的二级菜单
            //QueryWrapper<Rights> queryWrapper2 = new QueryWrapper<>();
            wrapper.clear();
            wrapper.eq("parent_id",oneRights.getId());
            List<MenuAdmin> twoList = menuAdminMapper.selectList(wrapper);
            oneRights.setChildren(twoList);
        }
        return oneList;
    }
}
