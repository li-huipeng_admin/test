package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.MenuMapper;
import com.jt.pojo.Menu;
import com.jt.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MenuServiceImpl implements MenuService{
    @Autowired
    private MenuMapper menuMapper;
    @Override
    public List<Menu> findMenusList() {
        //1.查询一级菜单数据
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",0);
        List<Menu> oneList =
                menuMapper.selectList(queryWrapper);
        //2.如何查询二级菜单  父子关系的封装!!!
        for (Menu oneRights : oneList){
            //查询该元素的二级菜单
            //QueryWrapper<Rights> queryWrapper2 = new QueryWrapper<>();
            queryWrapper.clear();
            queryWrapper.eq("parent_id",oneRights.getId());
            List<Menu> twoList = menuMapper.selectList(queryWrapper);
            oneRights.setChildren(twoList);
        }
        return oneList;
    }
}
