package com.jt.service;

import com.jt.pojo.Menu;
import com.jt.pojo.Rights;

import java.util.List;

public interface MenuService {

    List<Menu> findMenusList();
}
