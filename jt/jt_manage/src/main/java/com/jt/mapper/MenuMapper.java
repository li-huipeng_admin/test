package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Menu;

public interface MenuMapper extends BaseMapper<Menu> {
}
