package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Attendance;

public interface AttendanceMapper extends BaseMapper<Attendance> {
}
