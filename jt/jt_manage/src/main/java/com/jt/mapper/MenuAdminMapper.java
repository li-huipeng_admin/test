package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Menu;
import com.jt.pojo.MenuAdmin;

public interface MenuAdminMapper extends BaseMapper<MenuAdmin> {
}
