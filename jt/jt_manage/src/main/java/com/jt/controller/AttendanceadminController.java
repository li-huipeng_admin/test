package com.jt.controller;

import com.jt.pojo.Attendance;
import com.jt.service.AttendanceAdminService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("attendanceAdmin")
public class AttendanceadminController {
    @Autowired
    private AttendanceAdminService attendanceAdminService;

    @GetMapping("/attendance")
    public SysResult findAttendance(){
        List<Attendance> att=attendanceAdminService.findAttendance();
        return SysResult.success(att);
    }
}
