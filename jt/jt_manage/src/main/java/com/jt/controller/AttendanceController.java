package com.jt.controller;

import com.jt.pojo.Attendance;
import com.jt.service.AttendanceService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/attendance")
public class AttendanceController {

    @Autowired
    private AttendanceService attendanceService;

    @GetMapping("/list")
    private SysResult attendances(){
        List<Attendance> attendances = attendanceService.attendances();
        return SysResult.success(attendances);
    }
    @PutMapping("/attend")
    private SysResult attend(@RequestBody Attendance attendance){
        attendanceService.attend(attendance);
        return SysResult.success();
    }

}
