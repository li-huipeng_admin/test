package com.jt.controller;

import com.jt.pojo.Menu;
import com.jt.pojo.Rights;
import com.jt.service.MenuService;
import com.jt.service.RightsService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    /**
     * URL: /rights/getRightsList
     * 参数: null
     * 类型: get
     * 返回值: SysResult对象 List
     * 业务: 只查询前 2级权限
     */
    @GetMapping("/getMenuList")
    public SysResult getMenuList(){
        List<Menu> rightsList =
                menuService.findMenusList();
        return SysResult.success(rightsList);
    }
}
