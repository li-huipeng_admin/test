package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("attendance")
@Data
@Accessors(chain = true)
public class Attendance extends BasePojo{
    @TableId()
    private Integer id;
    private String username;
    private boolean one;
    private boolean two;
    private boolean three;
    private boolean four;
    private boolean five;
    private boolean six;
    private boolean seven;
    private boolean eight;
    private boolean nine;
    private boolean ten;
    private boolean eleven;
    private boolean twelve;
    private boolean thirteen;
    private boolean fourteen;
    private boolean fifteen;
    private boolean sixteen;
    private boolean seventeen;
    private boolean eightteen;
    private boolean nineteen;
    private boolean twenty;
    private boolean twentyone;
    private boolean twentytwo;
    private boolean twentythree;
    private boolean twentyfour;
    private boolean twentyfive;
    private boolean twentysix;
    private boolean twentyseven;
    private boolean twentyeight;
    private boolean twentynine;
    private boolean thirty;
    private boolean Thirtyone;




}
